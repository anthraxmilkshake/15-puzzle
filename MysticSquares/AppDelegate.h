//
//  AppDelegate.h
//  MysticSquares
//
//  Created by Mark Griffith on 2/8/14.
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
