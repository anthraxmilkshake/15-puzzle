//
//  Game.h
//  MysticSquares
//
//  Created by Mark Griffith on 2/8/14.
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Game : NSObject
-(void) gameReset;
-(BOOL) gameMakeMove: (NSString*) swipe;
-(BOOL) gameDidWin;
-(void) addThisButton:(UIButton*) button;
-(void) gameShuffleThisManyMoves:(int) moves;
@property (nonatomic) BOOL isResetting;
@end
