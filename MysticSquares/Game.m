//
//  Game.m
//  MysticSquares
//
//  Created by Mark Griffith on 2/8/14.
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "Game.h"
@interface Game ()
@property (nonatomic) NSMutableArray * gameBoard;
@property (nonatomic) int buttonCount;
@property (nonatomic) int blankX;
@property (nonatomic) int blankY;
@property (nonatomic) int animateCounteri;
@property (nonatomic) int animateCounterj;
@property (nonatomic) NSTimer * timer;
@property (nonatomic) NSMutableArray * solvedGameBoard;
@property (nonatomic) NSMutableArray * copiedBoard;

@property (nonatomic) int shuffleCounter;
@property (nonatomic) int shuffleMoves;
@property (nonatomic) NSString *lastShuffleMove;
-(void) swapThisButton: (UIButton*) button1 withThatButton: (UIButton*) button2;
- (void)timerFireMethod:(NSTimer *)timer;
@end

@implementation Game

-(NSMutableArray*) gameBoard
{
    if (!_gameBoard)
    {
        self.blankX = 3;
        self.blankY = 3;
    _gameBoard = [[NSMutableArray alloc]initWithCapacity:4];
    for (int i=0; i<4; i++)
        [_gameBoard addObject: [[NSMutableArray alloc]initWithCapacity:4]];
    
    _solvedGameBoard = [[NSMutableArray alloc]initWithCapacity:4];
    for (int i=0; i<4; i++)
        [_solvedGameBoard addObject: [[NSMutableArray alloc]initWithCapacity:4]];
    
    }
    return _gameBoard;
}
-(void) addThisButton: (UIButton*) button
{
    
    if (self.buttonCount <= 3)
    {
        self.gameBoard[self.buttonCount %4][0] = button;
        self.solvedGameBoard[self.buttonCount %4][0] = button;
    }
    else if (self.buttonCount <= 7)
    {
        self.gameBoard[self.buttonCount %4][1] =button;
        self.solvedGameBoard[self.buttonCount %4][1] = button;
    }
    else if (self.buttonCount <= 11)
    {
        self.gameBoard[self.buttonCount %4][2] = button;
        self.solvedGameBoard[self.buttonCount %4][2] = button;
    }
    else if (self.buttonCount <= 15)
    {
        self.gameBoard[self.buttonCount %4][3] = button;
        self.solvedGameBoard[self.buttonCount %4][3] = button;
    }
    self.buttonCount++;
    
}
-(void) swapThisButton: (UIButton*) button1 withThatButton: (UIButton*) button2
{
    int button1X;
    int button1Y;
    int button2X;
    int button2Y;
    for(int j=0; j<4; j++)
        for (int i=0;i<4;i++)
        {
            if (self.gameBoard[i][j] == button1)
            {   button1X = i;
                button1Y = j;
            }
            else if (self.gameBoard[i][j] == button2)
            {   button2X = i;
                button2Y = j;
            }
        }
    int tempXCoord = CGRectGetMidX([button1 frame]);
    int tempYCoord = CGRectGetMidY([button1 frame]);
    
    [UIView animateWithDuration:.3 animations:^
    {
    [button1 setCenter:CGPointMake(CGRectGetMidX([button2 frame]), CGRectGetMidY([button2 frame]))];
    [button2 setCenter:CGPointMake(tempXCoord,tempYCoord)];
    
    }];
    self.gameBoard[button1X][button1Y] = button2;
    self.gameBoard[button2X][button2Y] = button1;
    
}
-(void) gameReset
{
    if (self.isResetting ==true) {
        return;
    }
    else
        self.isResetting = true;
self.copiedBoard = [[NSMutableArray alloc] initWithArray:self.solvedGameBoard  copyItems:YES];
 
   self.timer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];


   self.blankX =3;
   self.blankY =3;
}
- (void)timerFireMethod:(NSTimer *)timer
{

        
    int i = self.animateCounteri;
    int j = self.animateCounterj;
    if (self.gameBoard[i][j]!=self.copiedBoard[i][j])
        [self swapThisButton: self.gameBoard[i][j] withThatButton:self.copiedBoard[i][j]];
    self.animateCounteri++;
    if(self.animateCounteri == 3 && self.animateCounterj == 3)
    {
        self.isResetting = false;
        self.copiedBoard = nil;
        self.animateCounteri=0;
        self.animateCounterj=0;
        [timer invalidate];
        return;
    }
    if (self.animateCounteri >= 4)
    {   self.animateCounteri = 0;
        self.animateCounterj++;
    }

}

-(void) gameShuffleThisManyMoves:(int) moves
{
    if (self.isResetting ==true) {
        return;
    }
    else
        self.isResetting = true;
    
    self.shuffleMoves = moves;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(timerFireMethodShuffle:) userInfo:nil repeats:YES];
    

}
- (void)timerFireMethodShuffle:(NSTimer *)timer
{
    if(self.shuffleCounter >=self.shuffleMoves)
    {
        self.isResetting = false;
        [self.timer invalidate];
        self.shuffleCounter = 0;
        return;
    }
    bool result;
    NSString * newShuffleMove;
    int picker = rand()%4;
    switch (picker) {
        case 0:
            if ([self.lastShuffleMove  isEqual: @"down"]) {
                result = false;
                break;
            }
            result = [self gameMakeMove:@"up"];
            newShuffleMove = @"up";
            break;
        case 1:
            if ([self.lastShuffleMove  isEqual: @"up"]) {
                result = false;
                break;
            }
            result = [self gameMakeMove:@"down"];
            newShuffleMove = @"down";
            break;
        case 2:
            if ([self.lastShuffleMove  isEqual: @"right"]) {
                result = false;
                break;
            }
            result = [self gameMakeMove:@"left"];
            newShuffleMove = @"left";
            break;
        case 3:
            if ([self.lastShuffleMove  isEqual: @"left"]) {
                result = false;
                break;
            }
            result = [self gameMakeMove:@"right"];
            newShuffleMove = @"right";
            break;
    }
    if (result == true) {
        self.lastShuffleMove = newShuffleMove;
        self.shuffleCounter++;
    }

    
}



- (BOOL) gameMakeMove: (NSString*) swipe
{
    UIButton *tempButton = self.gameBoard[self.blankX][self.blankY];
    if([swipe  isEqual: @"left"])
    {
        if(self.blankX ==3)
            return false;
        [self swapThisButton:self.gameBoard[self.blankX+1][self.blankY] withThatButton:tempButton];

        self.blankX++;
    }
    
    if([swipe  isEqual: @"right"])
    {
        if(self.blankX ==0)
            return false;
        
        [self swapThisButton:self.gameBoard[self.blankX-1][self.blankY] withThatButton:tempButton];
        self.blankX--;
    }
    
    if([swipe  isEqual: @"down"])
    {
        if(self.blankY ==0)
            return false;

        [self swapThisButton:self.gameBoard[self.blankX][self.blankY-1] withThatButton:tempButton];
        self.blankY--;
    }
    
    if([swipe  isEqual: @"up"])
    {
        if(self.blankY ==3)
            return false;

        [self swapThisButton:self.gameBoard[self.blankX][self.blankY+1] withThatButton:tempButton];
        self.blankY++;
    }
    self.gameBoard[self.blankX][self.blankY] = tempButton;
    return true;
}

- (BOOL) gameDidWin
{
    for(int j =0; j<4; j++)
        for(int i =0; i<4; i++)
            if(self.gameBoard[i][j] != self.solvedGameBoard[i][j])
                return false;
    return true;
}

@end
