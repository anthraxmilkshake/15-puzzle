//
//  ViewController.m
//  MysticSquares
//
//  Created by Mark Griffith on 2/8/14.
//  Copyright (c) 2014 Mark Griffith. All rights reserved.
//

#import "ViewController.h"
#import "Game.h"


@interface ViewController ()
//Buttons
@property (nonatomic) IBOutlet UIButton *buttonOne;
@property (nonatomic) IBOutlet UIButton *buttonTwo;
@property (nonatomic) IBOutlet UIButton *buttonThree;
@property (strong, nonatomic) IBOutlet UIButton *buttonFour;
@property (strong, nonatomic) IBOutlet UIButton *buttonFive;
@property (strong, nonatomic) IBOutlet UIButton *buttonSix;
@property (strong, nonatomic) IBOutlet UIButton *buttonSeven;
@property (strong, nonatomic) IBOutlet UIButton *buttonEight;
@property (strong, nonatomic) IBOutlet UIButton *buttonNine;
@property (strong, nonatomic) IBOutlet UIButton *buttonTen;
@property (strong, nonatomic) IBOutlet UIButton *buttonEleven;
@property (strong, nonatomic) IBOutlet UIButton *buttonTwelve;
@property (strong, nonatomic) IBOutlet UIButton *buttonThirteen;
@property (strong, nonatomic) IBOutlet UIButton *buttonFourteen;
@property (strong, nonatomic) IBOutlet UIButton *buttonFifteen;
@property (strong, nonatomic) IBOutlet UIButton *buttonBLANK;
@property (strong, nonatomic) IBOutlet UIButton *resetButton;
@property (strong, nonatomic) IBOutlet UIButton *shuffleButton;


//Swipes
@property (nonatomic)UISwipeGestureRecognizer *swipeRight;
@property (nonatomic)UISwipeGestureRecognizer *swipeLeft;
@property (nonatomic)UISwipeGestureRecognizer *swipeUp;
@property (nonatomic)UISwipeGestureRecognizer *swipeDown;

//Others
@property (nonatomic)Game * game;
@property (strong, nonatomic) IBOutlet UILabel *winLabel;
@property (strong, nonatomic) IBOutlet UISlider *slider;
@property (nonatomic) int shuffleAmount;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //Adjusting the View Appearance
    [self.view setBackgroundColor:[UIColor blackColor]];
    //Set Up Game
    self.game = [[Game alloc]init];
    
    //Configuring Buttons
    [self.buttonOne setBackgroundColor:[UIColor greenColor]];
    [self.buttonOne setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonOne];
    
    [self.buttonTwo setBackgroundColor:[UIColor greenColor]];
    [self.buttonTwo setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonTwo];
    
    [self.buttonThree setBackgroundColor:[UIColor greenColor]];
    [self.buttonThree setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonThree];
    
    [self.buttonFour setBackgroundColor:[UIColor greenColor]];
    [self.buttonFour setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonFour];
    
    [self.buttonFive setBackgroundColor:[UIColor greenColor]];
    [self.buttonFive setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonFive];
    
    [self.buttonSix setBackgroundColor:[UIColor greenColor]];
    [self.buttonSix setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonSix];
    
    [self.buttonSeven setBackgroundColor:[UIColor greenColor]];
    [self.buttonSeven setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonSeven];
    
    [self.buttonEight setBackgroundColor:[UIColor greenColor]];
    [self.buttonEight setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonEight];
    
    [self.buttonNine setBackgroundColor:[UIColor greenColor]];
    [self.buttonNine setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonNine];
    
    [self.buttonTen setBackgroundColor:[UIColor greenColor]];
    [self.buttonTen setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonTen];
    
    [self.buttonEleven setBackgroundColor:[UIColor greenColor]];
    [self.buttonEleven setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonEleven];
    
    [self.buttonTwelve setBackgroundColor:[UIColor greenColor]];
    [self.buttonTwelve setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonTwelve];
    
    [self.buttonThirteen setBackgroundColor:[UIColor greenColor]];
    [self.buttonThirteen setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonThirteen];
    
    [self.buttonFourteen setBackgroundColor:[UIColor greenColor]];
    [self.buttonFourteen setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonFourteen];
    
    [self.buttonFifteen setBackgroundColor:[UIColor greenColor]];
    [self.buttonFifteen setTintColor:[UIColor blackColor]];
    [self.game addThisButton: self.buttonFifteen];
    

    [self.game addThisButton: self.buttonBLANK];
    
    //Other Buttons
    [self.resetButton setBackgroundColor:[UIColor greenColor]];
    [self.resetButton setTintColor:[UIColor blackColor]];
    [self.winLabel setBackgroundColor:[UIColor greenColor]];

    [self.shuffleButton setBackgroundColor:[UIColor greenColor]];
    [self.shuffleButton setTintColor:[UIColor blackColor]];
    
    
    //swipe right
    self.swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    self.swipeRight.direction =UISwipeGestureRecognizerDirectionRight;
    
    [self.view addGestureRecognizer:self.swipeRight];
    
    //swipe left
    self.swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    self.swipeLeft.direction =UISwipeGestureRecognizerDirectionLeft;
    
    [self.view addGestureRecognizer:self.swipeLeft];
    
    //swipe up
    self.swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    self.swipeUp.direction =UISwipeGestureRecognizerDirectionUp;
    
    [self.view addGestureRecognizer:self.swipeUp];
    
    //swipe down
    self.swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    self.swipeDown.direction =UISwipeGestureRecognizerDirectionDown;
    
    [self.view addGestureRecognizer:self.swipeDown];
    
    //slider
    self.slider.minimumValue = 0;
    self.slider.maximumValue = 50;
    self.slider.minimumTrackTintColor = [UIColor greenColor];
    
}
- (IBAction)sliderSlid:(id)sender {
    UISlider *slider = (UISlider *)sender;
    self.shuffleAmount = slider.value;
    
}
- (IBAction)resetPressed:(id)sender {
    self.winLabel.hidden = true;
    [self.game gameReset];
    
}
- (IBAction)shufflePressed:(id)sender {
    self.winLabel.hidden = true;
    [self.game gameShuffleThisManyMoves:self.shuffleAmount];
    
}
- (void)swipeHandler:(UISwipeGestureRecognizer *)sender
{
    if([self.game isResetting]==false)
    {
    if (sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        [self.game gameMakeMove:@"right"];
        NSLog(@"swiped right");
    }
    if (sender.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        [self.game gameMakeMove:@"left"];
        NSLog(@"swiped left");
    }
    if (sender.direction == UISwipeGestureRecognizerDirectionUp)
    {
        [self.game gameMakeMove:@"up"];
        NSLog(@"swiped up");
    }
    if (sender.direction == UISwipeGestureRecognizerDirectionDown)
    {
        [self.game gameMakeMove:@"down"];
        NSLog(@"swiped down");
    }
    if([self.game gameDidWin]==true)
        self.winLabel.hidden = false;
    else
        self.winLabel.hidden = true;
    }
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
